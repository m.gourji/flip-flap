import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'your own API key',
  authDomain: 'own Domain',
  databaseURL: 'own Database',
  projectId: 'filp-flap',
  storageBucket: 'own Buket',
  messagingSenderId: 'own signIn ID',
  appId: 'own appId'
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
